<?php

namespace AppBundle\Service\Helper;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ObjectHelper
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var ValidatorInterface */
    private $validator;


    /**
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     */
    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function deserialize(string $content, $class, array $groups)
    {
        $context = ['groups' => $groups];
        if (!is_string($class)) {
            $context['object_to_populate'] = $class;
            $class = get_class($class);
        }

        return $this->serializer->deserialize($content, $class, 'json', $context);
    }

    public function validate($object, array $groups)
    {
        $errors = [];

        $violations = $this->validator->validate($object, null, $groups);
        if (count($violations) == 0) {
            return true;
        }

        // there are errors, now you can show them
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }
}
