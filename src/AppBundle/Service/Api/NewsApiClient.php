<?php

namespace AppBundle\Service\Api;

use GuzzleHttp\Client;

class NewsApiClient
{
    const BASE_URL = 'https://newsapi.org/v1/';

    /** @var Client */
    private $client;

    /** @var string */
    private $apiKey;

    /**
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client ?: $this->client = new Client();
    }

    private function get(string $endpoint, array $data)
    {
        $response = $this->getClient()->request('get', self::BASE_URL.$endpoint, [
            'query' => array_merge([
                'apiKey' => $this->apiKey,
            ], $data),
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getSources(string $language = 'en'): array
    {
        $response = $this->get('sources', [
            'language' => $language
        ]);

        return $response['sources'] ?? [];
    }

    public function getArticles(string $source): array
    {
        $response = $this->get('articles', [
            'source' => $source
        ]);

        return $response['articles'] ?? [];
    }
}
