<?php

namespace AppBundle\Doctrine\DBAL\Types;

use AppBundle\Entity\Sample;

class SampleEnumType extends AbstractEnumType
{
    protected $name = 'sampleEnum';
    protected $values = [Sample::VALUE_ONE, Sample::VALUE_TWO];
}
