<?php

namespace AppBundle\Doctrine\DBAL\Types;

use AppBundle\Entity\Extension\ConversionInterface;
use AppBundle\Model\AbstractGuideline;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\JsonType;

abstract class AbstractJsonType extends JsonType
{
    protected $name;

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!$value instanceof ConversionInterface) {
            return null;
        }

        return $value->toJson();
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (!is_array($value)) {
            $value = parent::convertToPHPValue($value, $platform);
        }

        if (!$value) {
            return null;
        }

        return $this->createFromArray($value);
    }

    private function createFromArray(array $data)
    {
        if (!isset($data['class'])) {
            return null;
        }

        if (!class_exists($data['class'])) {
            return null;
        }

        /** @var ConversionInterface $object */
        $object = new $data['class'];
        $object->fromArray($data);

        return $object;
    }

    public function getName()
    {
        return $this->name;
    }
}
