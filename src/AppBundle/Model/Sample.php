<?php

namespace AppBundle\Model;

use AppBundle\Entity\Extension\ConversionInterface;
use AppBundle\Entity\Extension\ConversionTrait;

class Sample implements ConversionInterface
{
    use ConversionTrait;

    /**
     * @var string
     */
    private $value;


    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Sample
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
