<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Extension\ConversionInterface;
use AppBundle\Entity\Extension\ConversionTrait;
use AppBundle\Entity\Extension\TimeTrackableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Model\Sample as SampleModel;

class Sample implements ConversionInterface
{
    use ConversionTrait;
    use TimeTrackableTrait;

    const VALUE_ONE = 'one';
    const VALUE_TWO = 'two';

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="sampleEnum")
     *
     * @Assert\NotBlank(groups={"create", "update"})
     */
    private $value;

    /**
     * @var SampleModel
     *
     * @ORM\Column(name="value", type="sampleJson")
     *
     * @Assert\NotBlank(groups={"create", "update"})
     */
    private $json;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Sample
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return SampleModel
     */
    public function getJson(): SampleModel
    {
        return $this->json;
    }

    /**
     * @param SampleModel $json
     *
     * @return Sample
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }
}
