<?php

namespace AppBundle\Entity\Extension;

trait ConversionTrait
{
    public function fromArray(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $setter = 'set'.ucfirst($key);
                if (method_exists($this, $setter)) {
                    $this->$setter($value);
                }
            }
        }
    }

    public function toJson()
    {
        $array = [
            'class' => static::class,
        ];

        $reflectionClass = new \ReflectionClass(static::class);
        foreach ($reflectionClass->getProperties() as $property) {
            if (preg_match('/\"(update|create)\"/', $property->getDocComment())) {
                $getter = 'get'.ucfirst($property->getName());
                if (is_callable([$this, $getter])) {
                    $array[$property->getName()] = $this->$getter();
                }
            }
        }

        return json_encode($array);
    }
}
