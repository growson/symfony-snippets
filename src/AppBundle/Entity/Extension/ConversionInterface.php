<?php

namespace AppBundle\Entity\Extension;

interface ConversionInterface
{
    public function fromArray(array $data);
    public function toJson();
}
