<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait RestTrait
{
    protected function createSuccessJsonResponse(array $data = null, string $message = null)
    {
        return new JsonResponse([
            'status' => 'OK',
            'data' => $data,
            'message' => $message,
        ]);
    }

    protected function createFailedJsonResponse(array $messages, int $statusCode = 400, array $data = null)
    {
        $response = new JsonResponse([
            'status' => 'ERROR',
            'code' => $statusCode,
            'data' => $data,
            'messages' => $messages,
        ], $statusCode);
        $response->setStatusCode($statusCode, json_encode($messages));

        return $response;
    }

    protected function createDataJsonResponse(array $data, array $query = null, array $meta = null)
    {
        return new JsonResponse([
            'status' => 'OK',
            'data' => $data,
            'query' => $query,
            'meta' => $meta,
        ]);
    }
}
